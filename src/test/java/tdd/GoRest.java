package tdd;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pojo_classes.*;
import utils.ConfigReader;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;

public class GoRest {

    Response response;
    Faker faker = new Faker();

    @BeforeTest
    public void beforeTest(){
        System.out.println("\nAPI test starting");
        RestAssured.baseURI = ConfigReader.getProperty("GoRestBaseURI");
    }

    @Test
    public void postTest(){

        Links links = Links
                .builder()
                .previous(faker.internet().url())
                .current(faker.internet().url())
                .next(faker.internet().url())
                .build();

        Pagination pagination = Pagination
                .builder()
                .total(100)
                .pages(34)
                .page(7)
                .limit(2)
                .links(links)
                .build();

        Meta meta = Meta
                .builder()
                .pagination(pagination)
                .build();

        DataRaw data1 = DataRaw
                .builder()
                .id(1)
                .postId(19)
                .name(faker.name().fullName())
                .email(faker.internet().emailAddress())
                .body(faker.internet().url())
                .build();

        DataRaw data2 = DataRaw
                .builder()
                .id(2)
                .postId(16)
                .name(faker.name().fullName())
                .email(faker.internet().emailAddress())
                .body(faker.internet().url())
                .build();

        DataRaw data3 = DataRaw
                .builder()
                .id(3)
                .postId(17)
                .name(faker.name().fullName())
                .email(faker.internet().emailAddress())
                .body(faker.internet().url())
                .build();

        ArrayList<DataRaw> data = new ArrayList<>(Arrays.asList(data1, data2, data3));


        MainData mainData = MainData
                        .builder()
                        .code(765)
                        .meta(meta)
                        .dataRaw(data)
                        .build();

                response = RestAssured
                        .given().log().all()
                        .contentType(ContentType.JSON)
                        .header("Authorisation", ConfigReader.getProperty("GoRestToken"))
                        .body(mainData)
                        .when().post("public-api/comments")
                        .then().log().all()
                        .assertThat().statusCode(200)
                        .extract().response();

        JsonPath jsonPath = response.jsonPath();

        assertThat(jsonPath.getList("data")).get(0).toString, Matchers.equalTo("{field=post message=must exist}");
        assertThat(jsonPath.getList("data")).get(1).toString, Matchers.equalTo("{field=post_id message=is not a number}");
        assertThat(jsonPath.getList("data")).get(2).toString, Matchers.equalTo("{field=email message=can't be blank, is invalid}");
        assertThat(jsonPath.getList("data")).get(3).toString, Matchers.equalTo("{field=body message=can't be blank}");
    }
}
