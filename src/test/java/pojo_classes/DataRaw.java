package pojo_classes;

import lombok.Builder;
import lombok.Data;

@Data
@Builder

public class DataRaw {

     private int id;
     private int postId;
     private String name;
     private String email;
     private String body;
}
