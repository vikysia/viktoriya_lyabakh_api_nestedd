package utils;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;


public class ExcelUtil {

    private static Logger logger = LogManager.getLogger(ExcelUtil.class);

    private static XSSFWorkbook workbook;
    private static XSSFSheet sheet;
    private static XSSFRow row;
    private static XSSFCell cell;
    private static String filePath;

    public static void openExcelFile(String fileName, String sheetName) {
        filePath = "test_data/" + fileName + ".xlsx";

        try {
            FileInputStream fileInputStream = new FileInputStream(filePath);
            workbook = new XSSFWorkbook(fileInputStream);
            logger.info("File " + fileName + " is exist!");
            sheet = workbook.getSheet(sheetName);
            logger.info("Sheet " + sheetName + " exist!");
        } catch (Exception e) {
            logger.debug(fileName + " and " + sheetName + " cannot be found");
        }
    }

    public static String getValue(int rowNumber, int cellNumber){
        row = sheet.getRow(rowNumber);
        cell = row.getCell(cellNumber);
        return cell.toString();
    }

    public static List<List<String>> getValue(){
        List<List<String>> allValue = new ArrayList<>();

        for(int r = sheet.getFirstRowNum()+1; r <= sheet.getLastRowNum(); r++){
            row = sheet.getRow(r);
            List<String> eachRow = new ArrayList<>();
            for(int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++){
                eachRow.add(getValue(r,c));
            }
            allValue.add(eachRow);
        }
        return allValue;
    }


}
